#include <sys/sysproto.h>

#define BULK_NET_INIT_SS(ss, nss) {int _i; for (_i=0;_i<nss;_i++) {(ss)[_i].rval = EINVAL;}}

struct sendto_s {
    struct sendto_args args;
    ssize_t rval;
};

struct recvfrom_s {
    struct recvfrom_args args;
    ssize_t rval;
};

static __inline u_long
_atomic_fetchsub_long(volatile u_long *p, u_long v)
{
        return atomic_fetchadd_long(p, -v);
}

static __inline void
_atomic_sub_long(volatile u_long *p, u_long v)
{
        atomic_add_long(p, -v);
        return;
}
