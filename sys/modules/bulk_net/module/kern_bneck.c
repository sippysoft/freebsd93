/*
 * Copyright (c) 2014 Sippy Software, Inc., http://www.sippysoft.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include <sys/cdefs.h>
#include <sys/types.h>
#include <machine/atomic.h>

#include "syscall.h"
#include "kern_bneck.h"

u_long
clk_tick(struct kern_bneck *kb)
{
    u_long clk;

    clk = atomic_fetchadd_long(&kb->ref_clk, 1);
    return (clk);
}

static inline long
get_tp_error_classI(struct kern_bneck *kb, struct trace_point *tp)
{
    u_long ref_clk, tp_clk;
    long err0r;

    tp_clk = atomic_fetchadd_long(&tp->tp_clk, 1);
    ref_clk = atomic_fetchadd_long(&kb->ref_clk, 0);

    err0r = (ref_clk & 0x1) ^ (tp_clk & 0x1);

    return (err0r);
}

static inline long
get_tp_error_classII(struct kern_bneck *kb, struct trace_point *tp)
{
    u_long ref_clk, tp_clk;
    long err0r;

    tp_clk = atomic_fetchadd_long(&tp->tp_clk, 1);
    ref_clk = atomic_fetchadd_long(&kb->ref_clk, 0);

    err0r = ref_clk - tp_clk;
    atomic_add_long(&tp->tp_clk, err0r);

    return (-err0r);
}

long
tp_tick(struct kern_bneck *kb, int tp_idx)
{
    struct trace_point *tp;
    long err0r;

    tp = &kb->tps[tp_idx];
    if (tp->detector_type == CLASSII_DETECTOR) {
        err0r = get_tp_error_classII(kb, tp);
    } else {
        err0r = get_tp_error_classI(kb, tp);
    }
    err0r = recfilter_apply(&tp->filter, err0r);
    return (err0r);
}

void
recfilter_init(struct recfilter_int *f, long fcoef, long scale, long scale_in, long initval)
{
    f->lastval = initval;
    f->a = scale - fcoef;
    f->b = fcoef;
    f->scale = scale;
    f->scale_in = scale_in;
    f->scale_out = scale_in / scale;
}

long
recfilter_apply(struct recfilter_int *f, long x)
{
    long lastval, nextval;

    lastval = atomic_fetchadd_long(&f->lastval, 0);

    nextval = (f->a * x * f->scale_in + f->b * lastval) / f->scale;
    atomic_add_long(&f->lastval, nextval - lastval);
    return (nextval / f->scale_out);
}

void
kern_bneck_init(struct kern_bneck *kb, int detector_type)
{
    int i;

    for (i = 0; i < 10; i++) {
        recfilter_init(&kb->tps[i].filter, 991, 1000, 1000000, 0);
        kb->tps[i].tp_clk = 1;
        kb->tps[i].detector_type = detector_type;
    }
}
