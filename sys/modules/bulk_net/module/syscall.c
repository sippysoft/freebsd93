/*-
 * Copyright (c) 1999 Assar Westerlund
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $FreeBSD: base/stable/9/share/examples/kld/syscall/module/syscall.c 193374 2009-06-03 09:28:58Z pjd $
 */

#include <sys/param.h>
#include <sys/proc.h>
#include <sys/module.h>
#include <sys/sysproto.h>
#include <sys/sysent.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/types.h>
#include <sys/file.h>
#include <sys/lock.h>
#include <sys/sx.h>

#include "syscall.h"
#include "kern_bneck.h"
#ifdef BULK_NETIO_DEBUG
#include "syscall_ver.h"
#endif

#define AS(name) (sizeof(struct name) / sizeof(register_t))

struct sendto_bulk_args {
    struct sendto_s *ss;
    uint64_t nss;
};

/*
 * The function for implementing the syscall.
 */

#ifdef BULK_NETIO_DEBUG
static volatile u_long sendto_bulk_cnt = 0;
static volatile u_long sendto_bulk_nss_tot = 0;
static struct kern_bneck sendto_bneck;
#endif

static int
sendto_bulk_syscall(struct thread *td, struct sendto_bulk_args *uap)
{
#ifdef BULK_NETIO_DEBUG
    u_long cnt, nss_tot;
    int nss_avg;
    long eval;
#endif
    struct sendto_s *bits;
    struct sendto_s smallbits[32];
    int error, i;
    u_int nfds;
    size_t ni;
    ssize_t rval;

#ifdef BULK_NETIO_DEBUG
    cnt = clk_tick(&sendto_bneck);
    nss_tot = atomic_fetchadd_long(&sendto_bulk_nss_tot, (u_long)uap->nss) + (u_long)uap->nss;

    eval = tp_tick(&sendto_bneck, 1);
    if ((cnt <= 10000 && cnt % 100 == 0) || (cnt > 10000 && cnt % 10000 == 0)) {
        printf(" -> sendto(%lu) eval0 = %ld\n", cnt, eval);
    }

    if (cnt < 10 || cnt % 100000 == 0) {
        nss_avg = nss_tot / (cnt + 1);
        printf("DEBUG: sendto_bulk_syscall: ss = %p, nss = %d, nss_avg = %d\n", uap->ss, (int)uap->nss, \
          nss_avg);
    }
#endif

    nfds = uap->nss;
    if (nfds > maxfilesperproc && nfds > FD_SETSIZE) {
        return (EINVAL);
    }
    ni = nfds * sizeof(struct sendto_s);
    if (ni > sizeof(smallbits))
        bits = malloc(ni, M_TEMP, M_WAITOK);
    else
        bits = smallbits;
    error = copyin(uap->ss, bits, ni);
    if (error)
        goto done;
    /* Iterate */
    for (i = 0; i < nfds; i++) {
#ifdef BULK_NETIO_DEBUG
        if (cnt < 10 || cnt % 100000 == 0)
            printf(" -> sendto(%d, %p, %d, %d)", bits[i].args.s, bits[i].args.buf, (int)bits[i].args.len, bits[i].args.flags);
#endif
        rval = sys_sendto(td, &bits[i].args);
#ifdef BULK_NETIO_DEBUG
        if (cnt < 10 || cnt % 100000 == 0)
            printf(" = %d\n", (int)rval);
#endif
        error = copyout(&rval, (uint8_t *)(&uap->ss[i]) + offsetof(struct sendto_s, rval), sizeof(rval));
        if (error)
            goto done;
    }
    error = i;

done:
    if (ni > sizeof(smallbits))
        free(bits, M_TEMP);
#ifdef BULK_NETIO_DEBUG
    eval = tp_tick(&sendto_bneck, 0);
    if ((cnt <= 10000 && cnt % 100 == 0) || (cnt > 10000 && cnt % 10000 == 0)) {
        printf(" -> sendto(%lu) eval = %ld\n", cnt, eval);
    }
#endif
    return (error);
}

struct recvfrom_bulk_args {
    struct recvfrom_s *ss;
    uint64_t nss;
};

#ifdef BULK_NETIO_DEBUG
static volatile u_long recvfrom_bulk_cnt = 0;
static volatile u_long recvfrom_bulk_nss_tot = 0;
#endif

static int
recvfrom_bulk_syscall(struct thread *td, struct recvfrom_bulk_args *uap)
{
#ifdef BULK_NETIO_DEBUG
    u_long cnt, nss_tot;
#endif
    struct recvfrom_s *bits;
    struct recvfrom_s smallbits[32];
    int error, i;
    u_int nfds;
    size_t ni;
    ssize_t rval;

#ifdef BULK_NETIO_DEBUG
    cnt = atomic_fetchadd_long(&recvfrom_bulk_cnt, (u_long)1);
    nss_tot = atomic_fetchadd_long(&recvfrom_bulk_nss_tot, (u_long)uap->nss) + (u_long)uap->nss;

    printf("recvfrom_bulk_syscall: ss = %p, nss = %d\n", uap->ss, (int)uap->nss);
#endif

    nfds = uap->nss;
    if (nfds > maxfilesperproc && nfds > FD_SETSIZE)
        return (EINVAL);
    ni = nfds * sizeof(struct recvfrom_s);
    if (ni > sizeof(smallbits))
        bits = malloc(ni, M_TEMP, M_WAITOK);
    else
        bits = smallbits;
    error = copyin(uap->ss, bits, ni);
    if (error)
        goto done;
    /* Iterate */
    for (i = 0; i < nfds; i++) {
#ifdef BULK_NETIO_DEBUG
        printf("recvfrom(%d, %p, %d, %d, %p, %p)", bits[i].args.s, bits[i].args.buf, (int)bits[i].args.len, bits[i].args.flags, \
          bits[i].args.from, bits[i].args.fromlenaddr);
#endif
        rval = sys_recvfrom(td, &bits[i].args);
#ifdef BULK_NETIO_DEBUG
        printf(" = %d\n", (int)rval);
#endif
        error = copyout(&rval, (uint8_t *)(&uap->ss[i]) + offsetof(struct recvfrom_s, rval), sizeof(rval));
        if (error)
            goto done;
    }
    error = i;

done:
    if (ni > sizeof(smallbits))
        free(bits, M_TEMP);
    return (error);
}

/*
 * The `sysent' for the new syscall
 */

static struct sysent net_bulk_sysent[2] = {
    {
     AS(sendto_bulk_args),		/* sy_narg */
     (sy_call_t *)sendto_bulk_syscall	/* sy_call */
    }, {
     AS(recvfrom_bulk_args),		/* sy_narg */
     (sy_call_t *)recvfrom_bulk_syscall	/* sy_call */
    }
};

/*
 * The offset in sysent where the syscall is allocated.
 */
static struct {
    int offset;
    struct sysent old_sysent;
    int reged;
    struct sysent *bulk_sysent;
    const char *syscall_name;
} mod_data[2] = {
    {.offset = NO_SYSCALL, .reged = 0, .bulk_sysent = &net_bulk_sysent[0], .syscall_name = "sendto_bulk"},
    {.offset = NO_SYSCALL, .reged = 0, .bulk_sysent = &net_bulk_sysent[1], .syscall_name = "recvfrom_bulk"}
};

/*
 * The function called at load/unload.
 */
static int
net_bulk_modevent(struct module *module, int cmd, void *arg)
{
	int error = 0, i;
	modspecific_t ms;

	switch (cmd) {
	case MOD_LOAD :
#ifdef BULK_NETIO_DEBUG
		printf("bulk_netio version %s loading...\n", BULK_NETIO_GIT_REVISION);
                kern_bneck_init(&sendto_bneck, CLASSI_DETECTOR);
#endif
		for (i = 0; i < 2; i++) {
			error = syscall_register(&mod_data[i].offset, mod_data[i].bulk_sysent, \
                          &mod_data[i].old_sysent);
#ifdef BULK_NETIO_DEBUG
			printf(" -> syscall %s(2) loaded at %d with error %d\n", \
                          mod_data[i].syscall_name, mod_data[i].offset, error);
#endif
			if (error == 0)
				mod_data[i].reged = 1;
		}
		bzero(&ms, sizeof(ms));
		ms.intval = mod_data[0].offset;
		MOD_XLOCK;
		module_setspecific(module, &ms);
		MOD_XUNLOCK;
		break;
	case MOD_UNLOAD :
#ifdef BULK_NETIO_DEBUG
		printf("bulk_netio version %s unloading...\n", BULK_NETIO_GIT_REVISION);
#endif
		for (i = 0; i < 2; i++) {
			if (!mod_data[i].reged)
				continue;
			syscall_deregister(&mod_data[i].offset, &mod_data[i].old_sysent);
#ifdef BULK_NETIO_DEBUG
			printf(" -> syscall %s(2) unloaded from %d\n", \
			  mod_data[i].syscall_name, mod_data[i].offset);
#endif
			mod_data[i].reged = 0;
		}
		break;
	default :
		error = EOPNOTSUPP;
		break;
	}
	return (error);
}

static moduledata_t net_bulk_mod = {
        "net_bulk",
        net_bulk_modevent,
        NULL,
};
DECLARE_MODULE(net_bulk, net_bulk_mod, SI_SUB_VFS, SI_ORDER_ANY);

/* So that loader and kldload(2) can find us, wherever we are.. */
MODULE_VERSION(net_bulk, 1);
